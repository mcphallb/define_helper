# Class: define_helper::packages
#
# This class collects all data from hiera for the class package. The
# default action is ensure is set to install.
#
# Parameters
#
# names: a hash of packages and their parameters
#
# Usage
#
# In hiera config:
#
# define_helper::packages:
#   uuid:
#     - ensure: installed
#   ntp:
#     - ensure: latest
#
#
class define_helper::packages (
  ) {
    $defaults = {
      ensure => 'installed',
    }

    $hiera_config = hiera_hash('define_helper::packages', undef)

    validate_hash( $hiera_config )

    create_resources(package, $hiera_config, $defaults)
}

