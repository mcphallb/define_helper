# Class: define_helper::rhnreg
#
# This class collects all data from hiera for the module 
# mthibaut/users. See http://forge.puppetlabs.com/mthibaut/users
# for more information.
#
# Parameters
#
# register: a hash of registration information
#
# Usage
#
# In hiera config:
#
# define_helper::rhnreg:
#   ${fqdn}:
#     activationkeys: 'lklkasdjjflakjsdfl'
#     server_url: https://example.com/XMLRPC'
#     ssl_ca_cert: '/path/to/TRUSTED_SSL_CERT'
#
#
class define_helper::rhnreg (
  $register = {}
  ) {
      validate_hash( $register )
      create_resources('rhn_register',$register)
}

