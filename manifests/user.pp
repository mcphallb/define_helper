# Class: define_helper::vhost
#
# This class collects all data from hiera for the module 
# mthibaut/users. See http://forge.puppetlabs.com/mthibaut/users
# for more information.
#
# Parameters
#
# groups:  A hash of all vhosts
#
# Usage
#
# In hiera config:
#
# define_helper::users::groups:
#   sysadmin:
#
# Remember, it's not the actual users you are specifying, just
# the name of the hash you wish to use. 
#
# Read http://forge.puppetlabs.com/mthibaut/users
#
class define_helper::user (
  $groups = {}
  ) {
      validate_hash( $groups )
        create_resources('users',$groups)
}

