# Class: define_helper::vhost
#
# This class collects all data from hiera for puppetlabs/apache vhost
# define type.
#
# Parameters
#
# vhosts:  A hash of all vhosts
#
# Usage
#
# define_helper::vhosts:
#   example.com:
#     port: 80
#     docroot: /var/www/example.com
class define_helper::vhost (
  $vhosts = {}
  ) {
      validate_hash( $vhosts )
        create_resources('apache::vhost',$vhosts)
}

