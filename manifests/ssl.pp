# Class: define_helper::vhost
#
# This class collects all data from hiera for puppetlabs/apache vhost
# define type.
#
# Parameters
#
# vhosts:  A hash of all vhosts
#
# Usage
#
# define_helper::ssl::names:
#     cn: name.nam3.msu.edu
class define_helper::ssl (
  $names = {}
  ) {
      validate_hash( $names )
        create_resources('ssl::cert',$names)
}

